<?php 

require_once "app/init.php";
// if($hash->verify('abcd1234', '$2y$10$cZwW.W5Qd9qjykYwzflECO.9Qp0vXsFNKbyqzXj9W2i2xivFpTHYC'))
// {
//     echo "correct!";
// }
// else{
//     echo "Wrong";
// }
//var_dump($database->query("SELECT * FROM contacts));
$auth->build(); //it automatically creates a table for me!
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Index</title>
</head>
<body>
    <?php if($auth->check()): ?>
    <p>You are signed in as <?= $auth->user()->username; ?>! <a href="signout.php">Sign Out!</a></p>
    <?php else: ?>
    <p>You are not signed in! <a href="signin.php">Sign In!</a> or <a href="signup.php">Sign Up!</a></p>
    <?php endif; ?>
</body>
</html>