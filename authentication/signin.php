<?php 
require_once "app/init.php";

if(!empty($_POST)){
    $username = $_POST['username'];
    $password = $_POST['password'];
    $rememberMe = $_POST['rem'] ?? null; 
    // from php 7 - short of ternary operator

    $validator = new Validator($database, $errorHandler);
    $validation = $validator->check($_POST, [
        'username' => [
            'required' => true
        ],
        'password' => [
            'required' => true
        ]
    ]);

    if($validation->fails()){
        //display the errors
        echo "<pre>", print_r($validation->errors()->all()), "</pre>";
    }else{
        $signin = $auth->signIn([
            'username' => $username,
            'password' => $password
        ]);
            
        if($signin){
            if($rememberMe){
                $token = $tokenHandler->createRememberMeToken($userHelper->getUserByUsername($username)->id);
                setcookie('token', $token, time()+1800); //time() returns time in milliseconds +1800 means 30minutes 
                //die(var_dump($token));
            }
            header('Location: index.php');
        }
    }
}

if(isset($_COOKIE['token']) && $tokenHandler->isValid($_COOKIE['token'], 1)){
    header('Location: index.php');
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign In</title>
</head>
<body>
    <form action="signin.php" method="POST">
        <fieldset>
            <legend>Sign In</legend>
            <label>
                User Name:
                <input type="text" name="username">
            </label>
            <label>
                Password:
                <input type="password" name="password">
            </label>
            <input type="checkbox" name="rem" checked="on">
            <input type="submit" value="Sign In">
        </fieldset>
    </form>
</body>
</html>