<?php
require_once("Sample.class.php");

//Wrong way!
// Sample $sampleObj = new Sample();

//Correct way
// $sampleObj = new Sample();
// $sampleObj->sayHello();

// $sampleObj->setFirstName("PHP is Weird!");
// echo $sampleObj->getFirstName();

// echo $sampleObj;
// unset($sampleObj);

// $book = new Book();
// $book->setName("Let us C!");
// echo $book->getName();

// $newBook = clone $book;
// $newBook->setName("Complete Reference!");
// echo $book->getName();

// $interfaces = class_implements('Book');
// if(isset($interfaces['NameInterface'])){
//     echo "Class Book has implemented the interface!";
// }
// else{
//     echo "Class Book hasn't implemented the interface!";
// }

// $person = new Person();
// $person->setName("Sahil Dalvi");
// echo $person->getName();

$base = new Base();
// $base->proVarOfBase =10;

$derived = new Derived();
$derived->insideBase();
$derived->insideDerived();
$derived->setProVar(10);
$derived->displayProVar();
?>