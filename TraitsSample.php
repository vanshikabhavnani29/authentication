<?php
trait Trait1{
    public function sayHello(){
        echo "Hello from Trait1";
    }
} 
trait Trait2{
    public function sayHello(){
        echo "Hello from Trait2";
    }
}
class  Sample
{
    use Trait1, Trait2{
        Trait1::sayHello insteadOf Trait2;
        Trait2::sayHello as sayHelloFromTrait2;
    }

    // public function sayHello(){
        // echo "Hello From Sample!";
    // }
}

$obj = new Sample();
$obj->sayHello();
$obj->sayHelloFromTrait2();
?>