<?php
require "app/init.php";
if(!empty($_POST)){
    $email = $_POST['email'];
    $token = $_POST['token'];
    $password = $_POST['password'];
    if($tokenHandler->isValid($token, 0)){
    $password_update_flag = $auth->resetUserPassword($token, $password);
    $token_update_flag = $tokenHandler->deleteToken($token);

    if($password_update_flag && $token_update_flag){
        header("Location: signin.php");
    }else{
        echo "Sorry, there was some issue while updating your password, please retry later!";
    }
}else{
    echo "<p>Your time to reset the password is expired!</p>";
}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reset Password</title>
</head>
<body>
    <?php
    if(isset($_GET['token']) && isset($_GET['email'])):
        $token = $_GET['token'];
        $email = $_GET['email'];

        if($tokenHandler->isValid($token, 0)):
    ?>
    <form action="reset-password.php" method="POST">
            <input type="hidden" name="token" value="<?=$token;?>">
            <label>
                Email:
                <input type="text" value="<?= $email;?>" name="email" readonly>
            </label>
            <br><br>
            <label>
                Password:
                <input type="password" name="password">
            </label>
            <br><br>
            <input type="submit" value="Reset My Password">
    </form>
<?php
    else:
        echo "<p>Something Fishy! I'll report to admin!";
    endif;
else:
?>
<p>how did you reach here??</p>
<?php
endif;
?>
</body>
</html>