<?php
class StudentModel{
    static function find($args){
        $query = "SELECT * FROM student WHERE ".$args['field']." ".$args['operator']." ". $args['value'];
        return $query;
    }

    static function __callStatic($method, $args){
        if(preg(match('/^findBy(.+)$/'), $method, $matches)){
            return static::find(array('field'=>$matches[1], 'value'=>$args[0], 'operator'=>$args[1]));
        }
    }
}

echo StudentModel::findById("Hello","like");
echo "<br>";
echo StudentModel::findByEmail("someone@example.com","=");
echo "<br>";
echo StudentModel::findByPhoneNumber("9802398023",">=");
echo "<br>";
echo StudentModel::findByCity("Mumbai","like");