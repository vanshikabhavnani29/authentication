<?php 
require_once "app/init.php";
if(!empty($_POST)){
    $email = $_POST['email'];
    $username = $_POST['username'];
    $password = $_POST['password'];

    $validator = new Validator($database, $errorHandler);
    $validation = $validator->check($_POST, [
        'email' => [
            'required' => true,
            'maxlength' => 255,
            'unique' => 'users',
            'email' => true
        ],
        'username' => [
            'required' => true,
            'minlength' => 2,
            'maxlength' => 20,
            'unique' => 'users'
        ],
        'password' => [
            'required' => true,
            'minlength' => 8 
        ]
    ]);

    if($validation->fails()){
        //display the errors
        echo "<pre>", print_r($validation->errors()->all()), "</pre>";
    }else{
        //create the user
        $created = $auth->create([
            'email' => $email,
            'username' => $username,
            'password' => $password
        ]);
        if($created){
            header("Location: index.php");
        }
    }
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign Up</title>
</head>
<body>
<form action="signup.php" method="POST">
        <fieldset>
            <legend>Sign Up</legend>
            <label>
                Email:
                <input type="text" name="email">
            </label>
            <?php 
            if($validation->errors()->has('email')):
            ?>
            <p class="danger">
            <?= $validation->errors()->first('email');?>
            </p>
            <?php endif; ?>
            <label>
                User Name:
                <input type="text" name="username">
            </label>
            <label>
                Password:
                <input type="password" name="password">
            </label>
            <input type="submit" value="Sign Up">
        </fieldset>
    </form>
</body>
</html>