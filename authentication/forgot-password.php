<?php
require "app/init.php";
if(!empty($_POST)){
    $email = $_POST['email'];
    $user = $userHelper->getUserByEmail($email);
    if($user){
        $token = $tokenHandler->createForgotPasswordToken($user->id);
        if($token){
            $mail->addAddress($user->email);
            $mail->Subject = "Reset Password";
            $mail->Body = "Use the below link within 15 minutes to reset your password, <br> <a href= 'http://localhost:8080/reset-password.php?token={$token}&email={$email}'>Reset Password</a>";
            if($mail->send()){
                echo "password reset link has been sent";
            }else{
                echo $mail->ErrorInfo;
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Forgot Password</title>
</head>
<body>
    <h1>Forgot Password</h1>
    <form action="forgot-password.php" method="POST">
        <fieldset>
            <legend>Forgot password</legend>
            <label>
                Email:
                <input type="text" name="email">
            </label>
            <input type="submit" value="Forgot Password">
        </fieldset>
    </form>
</body>
</html>