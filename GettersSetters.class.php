<?php
class GettersSetters{
    protected $__data = array('name'=>false, 'email'=>false);
    public function __get($property){
        if(!isset($this->__data[$property])){
            return false;
        }else{
            return $this->__data[$property];
        }
    }
    public function __set($property, $value){
        if(isset($this->__data[$property])){
            $this->__data[$property] = $value;
        }else{
            echo "you cannot set anything!";
        }
    }
    public function __isset($property){
        return isset($this->__data[$property]);
    }
    public function __unset($property){
        if(isset($this->__data[$property])){
            unset($this->__data[$property]);
        }
    }
}

$obj = new GettersSetters();
$obj->name = 'ABC';
echo $obj->name;
$obj->fakeProperty = 'Hello!';
echo $obj->fakeProperty;
//$var = 10;
//echo isset($var);
echo isset($obj->name);
unset($obj->name);
?>